# Homepage Builder Dockerfile #

### What is this repository for? ###

This is the Dockerfile for the Docker Hub repo here: https://hub.docker.com/repository/docker/jamesdesq/homepage_builder

It's used in a Bitbucket pipeline for building Angular applications and deploying them to AWS. 

If you want to test it out locally, you can run: 

`$ docker build -t homepage_builder .`

...then you can run a shell in the container using: 

`$ docker run -it --rm blog_build /bin/sh`