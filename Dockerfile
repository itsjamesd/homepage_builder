FROM alpine:latest

RUN apk add --update nodejs npm

RUN npm install -g @angular/cli

RUN apk add unzip

RUN apk add curl 

RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"

RUN unzip awscli-bundle.zip

RUN apk add py3-pip

RUN ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

RUN ln -s /usr/local/bin/aws aws